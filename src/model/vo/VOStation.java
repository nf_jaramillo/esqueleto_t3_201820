package model.vo;

import model.data_structures.UnicamenteIdentificado;

public class VOStation implements UnicamenteIdentificado{

	@Override
	public String darIdentificador() {
		
		return info;
	}
	
	//----------------------------------------
	//ATRIBUTOS
	//---------------------------
		private String info;
		
		public VOStation (String pInfo)
		{
			info = pInfo;
		}

}
