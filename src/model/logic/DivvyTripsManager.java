package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.Cola;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.ListaDoblementeEncadenada;
import model.data_structures.ListaSencillamenteEncadenada;
import model.data_structures.NodoListaDoble;
import model.data_structures.NodoListaSencilla;
import model.data_structures.Pila;
import model.data_structures.UnicamenteIdentificado;

public class DivvyTripsManager implements IDivvyTripsManager {
	
	//---------------------------------------------
	//Atributos
	//--------------------------------------------
		ListaDoblementeEncadenada<VOTrip> trips = new ListaDoblementeEncadenada<VOTrip>();
		ListaDoblementeEncadenada<UnicamenteIdentificado> stations = new ListaDoblementeEncadenada<UnicamenteIdentificado>();
		Cola<VOTrip> cola = new Cola();
		Pila<VOTrip> pila = new Pila();

	
	public void loadStations (String stationsFile) {
		CSVReader reader = null;
		try {
			reader = new CSVReader(new FileReader(stationsFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     String [] nextLine;
	     try {
	    	 int i = 0;
	    	 
			while ((nextLine = reader.readNext()) != null) {
				VOStation este = new VOStation(nextLine[0]);
				
				stations.add(0,este);
			    // nextLine[] is an array of values from the line
			    System.out.println(nextLine[0] + nextLine[1] + "etc...");
			 }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void loadTrips (String tripsFile) {
		CSVReader reader = null;
		try {
			reader = new CSVReader(new FileReader(tripsFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     String[]  nextLine;
	     try {
	    	 nextLine = reader.readNext();
			while ((nextLine = reader.readNext()) != null) {
					VOTrip este = new VOTrip(nextLine);
					trips.addAtEnd(este);
				    // nextLine[] is an array of values from the line
				  //  System.out.println(nextLine[0] + nextLine[1] + "etc...");
			 }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	
	public ListaDoblementeEncadenada<VOTrip> getTripsOfGender (String gender) {
		VOTrip temp = null;
		ListaDoblementeEncadenada<VOTrip> resp = new ListaDoblementeEncadenada<>();
		NodoListaSencilla<VOTrip> nodo = null;
		int cant = trips.size();
		if(cant>0)
		{
			nodo = trips.darNodo(0);
		}
		for (int i = 0; i < cant; i++) {
			
			temp = nodo.darElemento();
			String genero = temp.gender();
			if(temp!=null&&genero!=null&&genero.equals(gender))
			{
//				System.out.println(temp.gender());
				resp.add(0, temp);
			}
			nodo = nodo.darSiguiente();
		}
		return resp;
	}

	
	public ListaDoblementeEncadenada <VOTrip> getTripsToStation (int stationID) {
		VOTrip temp = null;
		ListaDoblementeEncadenada<VOTrip> resp = new ListaDoblementeEncadenada<>();
		NodoListaSencilla<VOTrip> nodo = null;
		int cant = trips.size();
		if(cant>0)
		{
			nodo = trips.darNodo(0);
		}
		
		for (int i = 0; i < cant; i++) {
			
			temp = nodo.darElemento();
			int station = temp.station2Id();
			if(temp!=null&&station==(stationID))
			{
//				System.out.println(temp.gender());
				resp.add(0, temp);
			}
			nodo = nodo.darSiguiente();
		}
		return resp;
	}	
	
	public void	loadBikeTrips(String bikeId)
	{
		NodoListaDoble<VOTrip> temp = null;
		VOTrip trip = null;
		int id = Integer.valueOf(bikeId);
		int size = trips.size();
		if(size>0)
		{
			temp = (NodoListaDoble<VOTrip>) trips.darNodo(0);
			if(temp.darElemento().darBikeId()==id) {
				
				cola.enqueue(temp.darElemento());
				pila.push(temp.darElemento());
			
			}
		}
		for (int i = 0; i < size-1; i++) {
			temp = (NodoListaDoble<VOTrip>) temp.darSiguiente();
			trip = temp.darElemento();
			if(trip.darBikeId()==id) {
				
				cola.enqueue(trip);
				pila.push(trip);
		
			}
		}
	}
	
	public void	loadBikeTrips(int stationId)
	{
		NodoListaDoble<VOTrip> temp = null;
		VOTrip trip = null;
		int size = trips.size();
		cola.clear();
		pila.clear();
		if(size>0)
		{
			temp = (NodoListaDoble<VOTrip>) trips.darNodo(0);
			if(temp.darElemento().station2Id()==stationId) {
				
				cola.enqueue(temp.darElemento());
				pila.push(temp.darElemento());
			
			}
		}
		for (int i = 0; i < size-1; i++) {
			temp = (NodoListaDoble<VOTrip>) temp.darSiguiente();
			trip = temp.darElemento();
			if(trip.station2Id()==stationId) {
				
				cola.enqueue(trip);
				pila.push(trip);
		
			}
		}
	}
	
	@Override
	public ListaDoblementeEncadenada getLastNStations (int bicycleId, int n) {
		loadBikeTrips(String.valueOf(bicycleId));
		ListaDoblementeEncadenada nombreEstaciones = new ListaDoblementeEncadenada();
		
		if(n==1)
		{
			VOStation c = new VOStation(cola.dequeue().getToStation());
			nombreEstaciones.add(c);
			n--;
		}
		else if(n>1)
		{
			VOTrip temp = cola.dequeue();
			VOStation c = new VOStation(temp.getToStation());
			nombreEstaciones.add(c);
			VOStation b = new VOStation(temp.getFromStation());
			nombreEstaciones.add(b);
			
			n = n-2;
		}
		
		for (int i = 0; i < n; i++) {
			
			VOStation b = new VOStation(cola.dequeue().getFromStation());
			nombreEstaciones.add(b);
			
			}
		
		return nombreEstaciones;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {
		loadBikeTrips(stationID);
		VOTrip resp = null;
		VOTrip temp = null;
		for (int i = 0; i < pila.size(); i++) {
			temp = pila.pop();
			if(i==n-1)
			{
				resp = temp;
			}
		}
		return resp;
		
	}	


}
