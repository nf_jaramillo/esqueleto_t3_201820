package model.data_structures;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


public class ListaSencillamenteEncadenada<E extends UnicamenteIdentificado> extends ListaEncadenadaAbstracta<E> 
{

	/**
	 * Constante de serializaci�n.
	 */
	private static final long serialVersionUID = 1L;
	
	private NodoListaDoble<E> ultimoNodo;

	
	/**
	 * Construye la lista vac�a.
	 * <b>post: </b> Se ha inicializado el primer nodo en null
	 */
	public ListaSencillamenteEncadenada() 
	{
		primerNodo = null;
		cantidadElementos = 0;
		ultimoNodo = null;
	}
	
    /**
     * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�metro. Actualiza el n�mero de elementos.
     * @param nPrimero el elemento a guardar en el primer nodo
     * @throws NullPointerException si el elemento recibido es nulo
     */
	public ListaSencillamenteEncadenada(E nPrimero)
	{
		if(nPrimero == null)
			throw new NullPointerException("Se recibe un elemento nulo");
		this.primerNodo = new NodoListaSencilla<E>(nPrimero);
		cantidadElementos = 1;
	}
	
	
	public void addAtEnd(E pElement) {
		NodoListaDoble<E> temp = new NodoListaDoble(pElement);
		if (size() == 0) {
			primerNodo = ultimoNodo = temp;
		} else {
			ultimoNodo.cambiarSiguiente(temp);
			ultimoNodo= temp;
		}
		cantidadElementos++;

	}

    /**
     * Agrega un elemento al final de la lista, actualiza el n�mero de elementos.
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
     * @param elem el elemento que se desea agregar.
     * @return true en caso que se agregue el elemento o false en caso contrario. 
     * @throws NullPointerException si el elemento es nulo
     */
	public boolean add(E elemento) 
	{
		 if(elemento == null)
	        {
	            throw new NullPointerException( );
	        }
	        
	        boolean agregado = false;
	        if(primerNodo == null)
	        {
	        	primerNodo = new NodoListaSencilla<E>( elemento );
	            agregado = true;
	            cantidadElementos++;
	        }
	        else
	        {
	            NodoListaSencilla<E> n = primerNodo;
	            boolean existe = false;
	            while(  n.darSiguiente( ) != null && !existe)
	            {
	                if(n.darElemento( ).darIdentificador( ).equals( elemento.darIdentificador( ) ))
	                {
	                    existe = true;
	                }
	                n = n.darSiguiente( );
	            }
	            if(!n.darElemento( ).darIdentificador( ).equals( elemento.darIdentificador( ) ))
	            {
	                n.cambiarSiguiente( new NodoListaSencilla<E>( elemento ) );
	                agregado = true;
	                cantidadElementos++;
	            }
	        }
	        return agregado;
	}

	/**
    * Agrega un elemento en la posici�n dada de la lista. Todos los elementos siguientes se desplazan.
    * Actualiza la cantidad de elementos.
    * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
    * @param pos la posici�n donde se desea agregar. Si pos es igual al tama�oo de la lista se agrega al final
    * @param elem el elemento que se desea agregar
    * @throws IndexOutOfBoundsException si el inidice es < 0 o > size()
    * @throws NullPointerException Si el elemento que se quiere agregar es null.
    */
	public void add(int index, E elemento) 
	{
		if(elemento == null)
        {
            throw new NullPointerException( );
        }
        NodoListaSencilla<E> nuevo = new NodoListaSencilla<E>( elemento );
//        if(!contains( elemento ))
        {
            
            if(index == 0)
            {
                nuevo.cambiarSiguiente( primerNodo );
                primerNodo = nuevo;
                cantidadElementos++;
            }
            else
            {
                NodoListaSencilla<E> n = primerNodo;
                int posActual = 0;
                while( posActual < (index-1) && n != null )
                {
                    posActual++;
                    n = n.darSiguiente( );
                }
                if(posActual != (index-1))
                {
                    throw new IndexOutOfBoundsException( );
                }
                nuevo.cambiarSiguiente( n.darSiguiente( ) );
                n.cambiarSiguiente( nuevo );
                cantidadElementos++;
            }
        }	
	}

	@Deprecated
	public ListIterator<E> listIterator() 
	{
		throw new UnsupportedOperationException ();
	}

	@Deprecated
	public ListIterator<E> listIterator(int index) 
	{
		throw new UnsupportedOperationException ();
	}

	
	/**
     * Elimina el nodo que contiene al objeto que llega por par�metro. Actualiza el n�mero de elementos.
     * @param objeto el objeto que se desea eliminar. objeto != null
     * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
     */
	public boolean remove(Object objeto) 
	{
		NodoListaSencilla<E> nodo = primerNodo;
		NodoListaSencilla<E> nodoA = null;
        boolean encontrado = false;
        while(nodo!=null&&!encontrado)
        {

            if(objeto==nodo.darElemento( ))
            {
            	if(nodoA==null)
            	{
            		
            		primerNodo = nodo.darSiguiente();
            		cantidadElementos--;
            	}
            	else if(nodo.darSiguiente() == null)
            	{
            		
            		nodoA.cambiarSiguiente(null);
            		cantidadElementos--;
            	}
            	else
            	{
                    encontrado = true;
                    nodoA.cambiarSiguiente(nodo.darSiguiente());
                    cantidadElementos--;
            	}
                
            } 
            nodoA = nodo;
            nodo = nodo.darSiguiente( );
        }
      
       return encontrado;
	}

	/**
     * Elimina el nodo en la posici�n por par�metro. Actualiza la cantidad de elementos.
     * @param pos la posici�n que se desea eliminar
     * @return el elemento eliminado
     * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
     */
	public E remove(int pos) 
	{
		NodoListaSencilla<E> nodo = primerNodo;
		NodoListaSencilla<E> nodoA = null;
        boolean encontrado = false;
        E objeto = null;
        try
        {
            objeto = get(pos);
            while(nodo!=null&&!encontrado)
            {

                if(objeto==nodo.darElemento( ))
                {
                	if(nodoA==null)
                	{
                		
                		primerNodo = nodo.darSiguiente();
                		cantidadElementos--;
                	}
                	else if(nodo.darSiguiente() == null)
                	{
                		
                		nodoA.cambiarSiguiente(null);
                		cantidadElementos--;
                	}
                	else
                	{
                        encontrado = true;
                        nodoA.cambiarSiguiente(nodo.darSiguiente());
                        cantidadElementos--;
                	}
                    
                } 
                nodoA = nodo;
                nodo = nodo.darSiguiente( );
            }
        }
        catch(IndexOutOfBoundsException e)
        {
        	throw e;
        }
      
       return objeto;
	}

	/**
     * Deja en la lista solo los elementos que est�n en la colecci�n que llega por par�metro. La cantidad de elementos se actualiza.
     * @param coleccion la colecci�n de elementos a mantener. coleccion != null
     * @return true en caso que se modifique (eliminaci�n) la lista o false en caso contrario
     */
	public boolean retainAll(Collection<?> coleccion) 
	{
		E[] lista = (E[]) coleccion.toArray();
		boolean resp  = false;
		NodoListaSencilla<E> nodoP = primerNodo;
		int i = 0;
		while(nodoP!=null&&i<lista.length)
		{
			if(lista[i]!=nodoP.darElemento())
			{
				remove(nodoP.darElemento());
				cantidadElementos--;
				resp = true;
			}
			nodoP = nodoP.darSiguiente();
			i++;
		}
		return resp;
		
	}

	 /**
     * Crea una lista con los elementos de la lista entre las posiciones dadas
     * @param inicio la posici�n del primer elemento de la sublista. Se incluye en la sublista
     * @param fin la posici�n del �tlimo elemento de la sublista. Se excluye en la sublista
     * @return una lista con los elementos entre las posiciones dadas
     * @throws IndexOutOfBoundsException Si inicio < 0 o fin >= size() o fin < inicio
     */
	public List<E> subList(int inicio, int fin) 
	{
		
		ListaSencillamenteEncadenada<E> resp = new ListaSencillamenteEncadenada<>();
		try
		{
			while(inicio<=fin)
			{
				resp.add(get(inicio));
				inicio++;
			}
		}
		catch(IndexOutOfBoundsException e)
		{
			throw e;
		}
		return resp;
	}

}
