package model.data_structures;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.sun.media.sound.UlawCodec;

import model.vo.VOTrip;



/**
 * Clase que representa la lista doblemente encadenada
 * @param <E> Tipo de los objetos que almacenar� la lista.
 */
public class ListaDoblementeEncadenada<E extends UnicamenteIdentificado> extends ListaEncadenadaAbstracta<E> implements IDoublyLinkedList<E>
{

	/**
	 *  Constante de serializaci�n
	 */
	private static final long serialVersionUID = 1L;
	
	private NodoListaDoble<E> ultimoNodo;

	 /**
     * Construye una lista vacia
     * <b>post:< /b> se ha inicializado el primer nodo en null
     */
	public ListaDoblementeEncadenada() 
	{
		primerNodo = null;
		ultimoNodo = null;
		
		cantidadElementos = 0;
	}
	
	/**
     * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
     * @param nPrimero el elemento a guardar en el primer nodo
     * @throws NullPointerException si el elemento recibido es nulo
     */
	public ListaDoblementeEncadenada(E nPrimero)
	{
		if(primerNodo == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		primerNodo = new NodoListaDoble<E>(nPrimero);
		cantidadElementos = 1;
	}
	
	/**
     * Agrega un elemento al final de la lista
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id.
     * Se actualiza la cantidad de elementos.
     * @param e el elemento que se desea agregar.
     * @return true en caso que se agregue el elemento o false en caso contrario. 
     * @throws NullPointerException si el elemento es nulo
     */
	public boolean add(E e) 
	{
		if(e == null)
        {
            throw new NullPointerException( );
        }
        
        boolean agregado = false;
        if(primerNodo == null)
        {
        	primerNodo = new NodoListaDoble<E>( e );
            agregado = true;
            cantidadElementos++;
        }
        else
        {
            NodoListaDoble<E> n = (NodoListaDoble<E>) primerNodo;
            boolean existe = false;
            while(  n.darSiguiente( ) != null && !existe)
            {
                if(n.darElemento( ).darIdentificador( ).equals( e.darIdentificador( ) ))
                {
                    existe = true;
                }
                n = (NodoListaDoble<E>) n.darSiguiente( );
            }
            if(!n.darElemento( ).darIdentificador( ).equals( e.darIdentificador( ) ))
            {
            	NodoListaDoble<E> nuevo = new NodoListaDoble<E>(e);
                n.cambiarSiguiente( nuevo );
                nuevo.cambiarAnterior(n);
                agregado = true;
                cantidadElementos++;
            }
        }
        return agregado;
	}

	/**
     * Agrega un elemento al final de la lista. Actualiza la cantidad de elementos.
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
     * @param elemento el elemento que se desea agregar.
     * @return true en caso que se agregue el elemento o false en caso contrario. 
     * @throws NullPointerException si el elemento es nulo
     */
	public void add(int index, E elemento) 
	{
		if(elemento == null)
        {
            throw new NullPointerException( );
        }
        NodoListaDoble<E> nuevo = new NodoListaDoble<E>( elemento );
//        if(!contains( elemento ))
        {
            
            if(index == 0)
            {
                nuevo.cambiarSiguiente( primerNodo );
                primerNodo = nuevo;
                if(nuevo.darSiguiente()!=null)
                {
                	((NodoListaDoble<E>) primerNodo.darSiguiente()).cambiarAnterior( (NodoListaDoble<E>) primerNodo);
                }
                cantidadElementos++;
            }
            else
            {
                NodoListaDoble<E> n = (NodoListaDoble<E>) primerNodo;
                int posActual = 0;
                while( posActual < (index-1) && n != null )
                {
                    posActual++;
                    n = (NodoListaDoble<E>) n.darSiguiente( );
                }
                if(posActual != (index-1))
                {
                    throw new IndexOutOfBoundsException( );
                }
                if(n.darSiguiente()!=null)
                {
                	 nuevo.cambiarSiguiente( n.darSiguiente( ) );
                     ((NodoListaDoble<E>) n.darSiguiente()).cambiarAnterior(nuevo);
                }
               
                n.cambiarSiguiente( nuevo );
                nuevo.cambiarAnterior(n);
                cantidadElementos++;
            }
        }		
	}
	
	public void addAtEnd(E pElement) {
		NodoListaDoble<E> temp = new NodoListaDoble(pElement);
		if (size() == 0) {
			primerNodo = ultimoNodo = temp;
		} else {
			ultimoNodo.cambiarSiguiente(temp);
			ultimoNodo= temp;
		}
		cantidadElementos++;

	}

	/**
	 * M�todo que retorna el iterador de la lista.
	 * @return el iterador de la lista.
	 */
	public ListIterator<E> listIterator() 
	{
		return new IteradorLista<E>((NodoListaDoble<E>) primerNodo);
	}

	/**
	 * M�todo que retorna el iterador de la lista desde donde se indica.
	 * @param index �ndice desde se quiere comenzar a iterar.
	 * @return el iterador de la lista.
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public ListIterator<E> listIterator(int index) 
	{
		if(index< 0 || index >= size())
			throw new IndexOutOfBoundsException("El �ndice buscado est� por fuera de la lista.");
		return new IteradorLista<E>((NodoListaDoble<E>) darNodo(index));
	}

	/**
     * Elimina el nodo que contiene al objeto que llega por par�metro.
     * Actualiza la cantidad de elementos.
     * @param objeto el objeto que se desea eliminar. objeto != null
     * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
     */
	
	public boolean remove(Object o) 
	{
		NodoListaDoble<E> nodo = (NodoListaDoble<E>) primerNodo;
		NodoListaDoble<E> nodoA = null;
        boolean encontrado = false;
        while(nodo!=null&&!encontrado)
        {

            if(o==nodo.darElemento( ))
            {
            	if(nodoA==null)
            	{
            		
            		primerNodo = (NodoListaDoble<E>) nodo.darSiguiente();
            		cantidadElementos--;
            		encontrado = true;
            	}
            	else if(nodo.darSiguiente() == null)
            	{
            		
            		nodoA.cambiarSiguiente(null);
            		cantidadElementos--;
            		encontrado = true;
            	}
            	else
            	{
                    encontrado = true;
                    if(nodoA.darSiguiente()!=null)
                    {
                    	nodoA.cambiarSiguiente(nodo.darSiguiente());
                        ((NodoListaDoble<E>) nodoA.darSiguiente()).cambiarAnterior(nodoA);
                    }
                    
                    cantidadElementos--;
            	}
                
            } 
            nodoA = nodo;
            nodo = (NodoListaDoble<E>) nodo.darSiguiente( );
        }
      
       return encontrado;
       
	}

	/**
     * Elimina el nodo en la posici�n por par�metro.
     * Actualiza la cantidad de elementos.
     * @param pos la posici�n que se desea eliminar
     * @return el elemento eliminado
     * @throws IndexOutOfBoundsException si index < 0 o index >= size()
     */
	public E remove(int index) 
	{
		try
		{
			E resp = get(index);
			remove(resp);
			return resp;
		}
		catch(IndexOutOfBoundsException e)
		{
			throw e;
		}

	}

	/**
     * Deja en la lista solo los elementos que est�n en la colecci�n que llega por par�metro.
     * Actualiza la cantidad de elementos
     * @param coleccion la colecci�n de elementos a mantener. coleccion != null
     * @return true en caso que se modifique (eliminaci�n) la lista o false en caso contrario
     */
	public boolean retainAll(Collection<?> c) 
	{
		E[] lista = (E[]) c.toArray();
		boolean resp  = false;
		NodoListaDoble<E> nodoP = (NodoListaDoble<E>) primerNodo;
		int i = 0;
		while(nodoP!=null&&i<lista.length)
		{
			if(lista[i]!=nodoP.darElemento())
			{
				remove(nodoP.darElemento());
				cantidadElementos--;
				resp = true;
			}
			nodoP = (NodoListaDoble<E>) nodoP.darSiguiente();
			i++;
		}
		return resp;
	}

	 /**
     * Crea una lista con los elementos de la lista entre las posiciones dadas
     * @param inicio la posici�n del primer elemento de la sublista. Se incluye en la sublista
     * @param fin la posici�n del �tlimo elemento de la sublista. Se excluye en la sublista
     * @return una lista con los elementos entre las posiciones dadas
     * @throws IndexOutOfBoundsException Si inicio < 0 o fin >= size() o fin < inicio
     */
	public List<E> subList(int inicio, int fin) 
	{
		ListaDoblementeEncadenada<E> resp = new ListaDoblementeEncadenada<>();
		try
		{
			while(inicio<=fin)
			{
				resp.add(get(inicio));
				inicio++;
			}
		}
		catch(IndexOutOfBoundsException e)
		{
			throw e;
		}
		return resp;
	}
	
	/**
	 * @return retorna la cantidad de elementos
	 */
	public int darCantidadElementos()
	{
		return cantidadElementos;
	}
	
}
