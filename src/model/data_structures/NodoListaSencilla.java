package model.data_structures;

import java.io.Serializable;

public class NodoListaSencilla<E extends UnicamenteIdentificado> implements Serializable
{

	/**
	 * Constante de Serializaci�n
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Elemento almacenado en el nodo.
	 */
	protected E elemento;
	
	/**
	 * Siguiente nodo.
	 */
	private NodoListaSencilla<E> siguiente;
	
	/**
	 * Constructor del nodo.
	 * @param elemento El elemento que se almacenar� en el nodo. elemento != null
	 */
	public NodoListaSencilla(E pElemento)
	{
		elemento = pElemento;
		siguiente = null;
	}
	
	/**
	 * M�todo que cambia el siguiente nodo.
	 * <b>post: </b> Se ha cambiado el siguiente nodo
	 * @param siguiente El nuevo siguiente nodo
	 */
	public void cambiarSiguiente(NodoListaSencilla<E> pSiguiente)
	{
		siguiente = pSiguiente;
	}
	
	/**
	 * M�todo que retorna el elemento almacenado en el nodo.
	 * @return El elemento almacenado en el nodo.
	 */
	public E darElemento()
	{
		return elemento;
	}
	
	/**
	 * Cambia el elemento almacenado en el nodo.
	 * @param elemento El nuevo elemento que se almacenar� en el nodo.
	 */
	public void cambiarElemento(E pElemento)
	{
		elemento = pElemento;
	}
	
	/**
	 * M�todo que retorna el identificador del nodo.
	 * Este identificador es el identificador del elemento que almacena.
	 * @return Identificador del nodo (identificador del elemento almacenado).
	 */
	public String darIdentificador()
	{
		return elemento.darIdentificador();
	}
	
	/**
	 * M�todo que retorna el siguiente nodo.
	 * @return Siguiente nodo
	 */
	public NodoListaSencilla<E> darSiguiente()
	{
		return siguiente;
	}

}
