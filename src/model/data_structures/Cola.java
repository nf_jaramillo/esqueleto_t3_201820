package model.data_structures;

public class Cola<E extends UnicamenteIdentificado> extends ListaSencillamenteEncadenada<E> implements IQueue<E>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//-----------------------------------------
	//Atributos
	//-------------------------------------------
	
	private NodoListaSencilla<E> ultimo;
	
	

	@Override
	public void enqueue(E t) {
		addAtEnd(t);
		     
	}

	@Override
	public E dequeue() {
		E temp = get(0);
		remove(0);
		return temp;
		
	}
	

}
