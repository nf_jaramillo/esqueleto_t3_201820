package model.data_structures;

import java.io.Serializable;

/**
 * Contrato que deben cumplir los elementos identificados de forma �nica.
 *
 */
public interface UnicamenteIdentificado extends Serializable 
{
	/**
	 * M�todo que retorna el identificador �nico del elemento.
	 * @return
	 */
	public String darIdentificador();
	

}
