package model.data_structures;

public class Pila <E extends UnicamenteIdentificado> extends ListaSencillamenteEncadenada<E> implements IStack<E>{

	@Override
	public void push(E t) {
		add(0,t);
		
	}

	@Override
	public E pop() {
		E temp = get(0);
		remove(0);
		return temp;
	}

}
