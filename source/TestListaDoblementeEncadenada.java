

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import model.data_structures.ListaDoblementeEncadenada;
import model.data_structures.UnicamenteIdentificado;
import model.vo.VOTrip;

public class TestListaDoblementeEncadenada {
	
	//-----------------------------
	//Atributos
	//-------------------------------
	private ListaDoblementeEncadenada<UnicamenteIdentificado> lista;
	private VOTrip elem;
	
	

	/**
	 * Genera el escenario 0
	 */
	
	private void setupEscenario0()
	{
		
		lista = new ListaDoblementeEncadenada<>();
	}
	
	/**
	 * Genera el escenario 1
	 */
	
	private void setupEscenario1()
	{
		lista = new ListaDoblementeEncadenada<>();
		elem = new VOTrip("17536701,12/31/2017 23:58,1/1/2018 0:03,3304,284,159,Claremont Ave & Hirsch St,69,Damen Ave & Pierce Ave,Subscriber,Male,1988");
		lista.add(elem);
		
	}
	
	@Test
	/**
	 * test de a�adir normal
	 */
	public void testAddNormal()
	{
		setupEscenario0();
		
		lista.add(new VOTrip("17536701,12/31/2017 23:58,1/1/2018 0:03,3304,284,159,Claremont Ave & Hirsch St,69,Damen Ave & Pierce Ave,Subscriber,Male,1988"));
		
		assertSame("No se puede a�adir normal",1,lista.size());
	}

	@Test
	/**
	 * test de a�adir con posicion
	 */
	public void testAddConPosicion()
	{
		setupEscenario0();
		
		lista.add(0,new VOTrip("17536701,12/31/2017 23:58,1/1/2018 0:03,3304,284,159,Claremont Ave & Hirsch St,69,Damen Ave & Pierce Ave,Subscriber,Male,1988"));
		
		assertSame("No se puede a�adir con posicion",1,lista.size());
	}
	
	@Test
	/**
	 * test de remover normal
	 */
	public void testRemoverNormal()
	{
		setupEscenario1();
		
		lista.remove(0);
		
		assertSame("No se puede remover normal",0,lista.size());
	}
	

	@Test
	/**
	 * test de remover normal por objeto
	 */
	public void testRemoverPorObjeto()
	{
		setupEscenario1();
		
		lista.remove(elem);
		
		assertSame("No se puede remover por objeto",0,lista.size());
	}
	
	@Test
	/**
	 * test de limpiar
	 */
	public void testLimpiar()
	{
		setupEscenario1();
		
		lista.clear();
		
		assertSame("No se puede limpiar",0,lista.size());
	}
	
	@Test
	/**
	 * test de cantidad de elementos
	 */
	public void testCantidadElementos()
	{
		setupEscenario1();
		
		
		assertSame("No se contar la cantidad de elementos",1,lista.size());
	}
	
	@Test
	/**
	 * test de contiene
	 */
	public void testContiene()
	{
		setupEscenario1();
		
		
		
		assertTrue("No seirve el metodo contains",lista.contains(elem));
	}
	
	@Test
	/**
	 * test de get por posicion
	 */
	public void testGetPorPosicion()
	{
		setupEscenario1();
		
		
		
		assertSame("No obtener por posicion",elem,lista.get(0));
	}
	
	@Test
	/**
	 * test de get Nodo
	 */
	public void testGetNodo()
	{
		setupEscenario1();
		
	
		
		assertSame("No obtener el nodo",elem,lista.darNodo(0).darElemento());
	}
	
	@Test
	/**
	 * test de get Nodo Anterior
	 */
	public void testGetNodoAnterior()
	{
		setupEscenario1();
		
		VOTrip elem2 = new VOTrip("17536700,12/31/2017 23:54,1/1/2018 0:18,5975,1402,145,Mies van der Rohe Way & Chestnut St,145,Mies van der Rohe Way & Chestnut St,Customer,,");
		
		lista.add(elem2);
		
		assertSame("No obtener el nodo anterior",elem,lista.darNodoAnterior(1).darElemento());
	}

}
