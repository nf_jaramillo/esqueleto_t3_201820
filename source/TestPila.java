import static org.junit.Assert.assertSame;

import org.junit.Test;

import model.data_structures.Cola;
import model.data_structures.Pila;
import model.vo.VOTrip;

public class TestPila {
	
	//-----------------------------
			//Atributos
			//-------------------------------
			private Pila lista;
			private VOTrip elem;
			
			/**
			 * Genera el escenario 0
			 */
			
			private void setupEscenario0()
			{
				
				lista = new Pila<>();
			}
			
			/**
			 * Genera el escenario 1
			 */
			
			private void setupEscenario1()
			{
				lista = new Pila();
				elem = new VOTrip("17536701,12/31/2017 23:58,1/1/2018 0:03,3304,284,159,Claremont Ave & Hirsch St,69,Damen Ave & Pierce Ave,Subscriber,Male,1988");
				lista.add(elem);
				
			}
			
			
			
			@Test
			/**
			 * test de push
			 */
			public void testPush()
			{
				setupEscenario0();
				
				lista.push(new VOTrip("17536701,12/31/2017 23:58,1/1/2018 0:03,3304,284,159,Claremont Ave & Hirsch St,69,Damen Ave & Pierce Ave,Subscriber,Male,1988"));
				
				assertSame("No se puede hacer push",1,lista.size());
			}
			
			@Test
			/**
			 * test de pop
			 */
			public void testPop()
			{
				setupEscenario1();
				
				lista.pop();
				assertSame("No se puede hacer pop",0,lista.size());
			}

}
