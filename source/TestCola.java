import static org.junit.Assert.assertSame;

import org.junit.Test;

import model.data_structures.Cola;
import model.data_structures.ListaDoblementeEncadenada;
import model.data_structures.UnicamenteIdentificado;
import model.vo.VOTrip;

public class TestCola {
	
	//-----------------------------
		//Atributos
		//-------------------------------
		private Cola lista;
		private VOTrip elem;
		
		/**
		 * Genera el escenario 0
		 */
		
		private void setupEscenario0()
		{
			
			lista = new Cola<>();
		}
		
		/**
		 * Genera el escenario 1
		 */
		
		private void setupEscenario1()
		{
			lista = new Cola();
			elem = new VOTrip("17536701,12/31/2017 23:58,1/1/2018 0:03,3304,284,159,Claremont Ave & Hirsch St,69,Damen Ave & Pierce Ave,Subscriber,Male,1988");
			lista.add(elem);
			
		}
		
		
		
		@Test
		/**
		 * test de enqueue
		 */
		public void testEnqueue()
		{
			setupEscenario0();
			
			lista.enqueue(new VOTrip("17536701,12/31/2017 23:58,1/1/2018 0:03,3304,284,159,Claremont Ave & Hirsch St,69,Damen Ave & Pierce Ave,Subscriber,Male,1988"));
			
			assertSame("No se puede hacer enque",1,lista.size());
		}
		
		@Test
		/**
		 * test de dequeue
		 */
		public void testDequeue()
		{
			setupEscenario1();
			
			lista.dequeue();
			assertSame("No se puede hacer dequeue",0,lista.size());
		}

}
